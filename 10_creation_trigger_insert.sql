USE R_K_module_4
GO

CREATE TRIGGER trig_INS
ON voucher
AFTER INSERT
AS
INSERT INTO voucher_log (Id, firstname_guest, surname_guest, age, country, passport_number, phone, email, 
name_apartmany, number_of_person, adults, children, arrival, departure, payment_method, amount_to_pay, type_of_operation)
SELECT Id, firstname_guest, surname_guest, age,country, passport_number, phone, email,
 name_apartmany, number_of_person, adults, children, arrival, departure, payment_method, amount_to_pay, 'INS'
FROM INSERTED


--checking
INSERT INTO voucher (firstname_guest, surname_guest, age, country, passport_number, phone, email,
name_apartmany, number_of_person, adults, children, arrival, departure, payment_method, amount_to_pay)
VALUES('sonya', 'connor', 25, 'the USA', 'yb2254732i', '0344521950046', 'ter22@mail.us', 'rainy day', '5', '2', '3', '20170612', '20170630', 'card', 1250)
 
SELECT * FROM voucher
SELECT * FROM voucher_log