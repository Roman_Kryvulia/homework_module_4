USE [education]
GO
CREATE SCHEMA [R_Kryvulia]
GO

CREATE SYNONYM voucher  
FOR R_K_module_4.dbo.voucher  
GO 

CREATE SYNONYM voucher_log  
FOR R_K_module_4.dbo.voucher_log  
GO 


SELECT * FROM voucher 

SELECT * FROM voucher_log


SELECT surname_guest, age, phone 
FROM voucher  
WHERE age > 25  
GO

SELECT id, surname_guest, country, type_of_operation 
FROM voucher_log  
WHERE type_of_operation = 'DEL'  
GO


