
USE R_K_module_4
GO

ALTER TABLE [voucher]
 ADD CONSTRAINT CK_voucher_age CHECK (age>=18)
ALTER TABLE [voucher]
 ADD CONSTRAINT CK_voucher_adults CHECK (adults>0)
ALTER TABLE [voucher]
 ADD CONSTRAINT CK_number_of_person CHECK (number_of_person = adults + children)
ALTER TABLE [voucher]
 ADD CONSTRAINT CK_arrival_daparture CHECK (departure >=arrival)

 
 --unsucceful insert (CHECK age>=18)

INSERT INTO voucher VALUES 
('anna', 'svenson', 17, 'sweden', 'ma121415', '+03412075349', 'ansvenson@gmail.com', 'sun rose', 3, 2, 1, '20180620', '20180701', 'card', '570')
GO
--successful insert (changed age)
INSERT INTO voucher VALUES 
('anna', 'svenson', 18, 'sweden', 'ma121415', '+03412075349', 'ansvenson@gmail.com', 'sun rose', 3, 2, 1, '20180620', '20180701', 'card', '570')
GO
SELECT * FROM voucher


--unsuccessful insert (CHECK adults>0)

INSERT INTO voucher VALUES 
('sonya', 'cooper', 25, 'england', 'uk36217f', '+441207534932', 'sonka@gmail.com', '#3009', 1, 0, 1, '20180620', '20180701', 'card', '420')
GO
--successful insert (changed ageadults)
INSERT INTO voucher VALUES 
('sonya', 'cooper', 25, 'england', 'uk36217f', '+441207534932', 'sonka@gmail.com', '#3009', 1, 1, 0, '20180620', '20180701', 'card', '420')
GO
SELECT * FROM voucher


--unsuccessful insert (CHECK number_of_person = adults + children)
INSERT INTO voucher VALUES 
('daniel', 'pfeiffer', 47, 'germany', 'sa321713er', '+03512075349', 'dan277@gmail.com', 'fun rabbit', 2, 2, 1, '20180720', '20180804', 'cash', '1200')
GO
--successful insert (changed  number_of_person)
INSERT INTO voucher VALUES 
('daniel', 'pfeiffer', 47, 'germany', 'sa321713er', '+03512075349', 'dan277@gmail.com', 'fun rabbit', 3, 2, 1, '20180720', '20180804', 'cash', '1200')
GO
SELECT * FROM voucher

--unsuccessful insert (CHECK departure >=arrival)
INSERT INTO voucher VALUES 
('ola', 'kowalska', 22, 'poland', 'kr321713', '+03412075349', 'ola777@gmail.com', 'blue moon', 2, 1, 1, '20180720', '20180701', 'card', '870')
GO
--successful insert (changed  departure)
INSERT INTO voucher VALUES 
('ola', 'kowalska', 22, 'poland', 'kr321713', '+03412075349', 'ola777@gmail.com', 'blue moon', 2, 1, 1, '20180620', '20180701', 'card', '870')
GO
SELECT * FROM voucher


--successful insert (UQ  email)

INSERT INTO voucher VALUES 
('olga', 'kkostevych', 38, 'ukraine', 'fe451717', '+038955075349', 'ola777@gmail.com', '#1405', 2, 2, 0, '20180725', '20180808', 'card', '1300')
GO
--successful insert (changed email)
INSERT INTO voucher VALUES 
('olga', 'kostevych', 38, 'ukraine', 'fe451717', '+038955075349', 'ola77@gmail.com', '#1405', 2, 2, 0, '20180725', '20180808', 'card', '1300')
GO
SELECT * FROM voucher
